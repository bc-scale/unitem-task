# Unitem-task

Multithread application producing, processing and saving images.
Invocation example:

```console
poetry run prod-cons --n-imgs 10 --width 1024 --height 512 --log-lvl DEBUG
```

Application accepts following arguments:

- `--n-imgs` - number of images to produce,
- `--width` - width of produced images
- `--height` - height of produced images,
- `--log-lvl` - logging level.

To get more information about application and default arguments run:

```console
poetry run prod-cons --help
```

## How to run

Using poetry script definition:

```console
poetry run prod-cons
```

Pointing application entry point:

```console
poetry run python src/main.py
```

Using Makefile (only with predefined arguments):

```console
make run
```

Using docker.

### Dockerizing script

The easiest way to run application using docker is to run following script, which accepts same arguments as application:

```console
./scripts/run_in_docker.sh
```

It will build `unitem-task` image (if not already built) and run it with *preprocessed* directory as named volume. Be aware that this script won't override the image.

### Docker build and run

You can also build image by your own running:

```console
docker build . -t unitem-task
```

or using Makefile command:

```console
make docker_build
```

Then you can run dockerized application. Remember to mount host directory (*processed* recommended) with `/app/processed`. Use interactive mode to see logs in your console.

```console
docker run -v $(pwd)/processed:/app/processed -it unitem-task
```

## Structure

- src - application code
  - src/main.py - entry point
  - src/service - primary application package
  - src/tests - tests
    - src/tests/data - examples of input and expected output images
  - src/utils - repeatedly used functions (i.e. logging)
- processed - default (and recommended) output images store directory
- scripts - additional scripts
  - example_data_generator.py - generates images (input and expected output) for unit tests
  - run_in_docker.sh - run dockerized application; for more info check [Dockerizing script](#Dockerizing-script) section

## Makefile

Below are listed most important Makefile commands.
To start your work, you need to set up your local environment and hooks.

```console
make install_dev
```

To format code with `isort` and `black` use:

```console
make format
```

To build project use:

```console
make build
```

To run coverage tests use:

```console
make test_cov
```

This will create `coverage.xml`, `junit/test-results.xml` summury files and `htmlcov` directory. Open in browser `htmlcov/index.html` to see how tests covered code.
