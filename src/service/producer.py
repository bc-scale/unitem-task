import logging
import time
from queue import Queue
from typing import Final

from PIL.Image import Image, fromarray

from src.service.actor import Actor
from src.service.source import Source

logger = logging.getLogger("custom_logger")
SLEEP_SEC: Final[float] = 0.05


class Producer(Actor):
    """Thread taking a data from source, casting to *Pillow Image*
    and passing it via queue *A*."""

    def __init__(
        self,
        source: Source,
        queue_A: Queue[Image],
        err_queue: Queue[Exception],
        n_iterations: int,
        timeout: int = 2,
    ):
        super().__init__(
            role="producer",
            err_queue=err_queue,
            n_iterations=n_iterations,
            timeout=timeout,
        )
        self._queue_A = queue_A
        self._source = source

    def _action(self) -> None:
        img = fromarray(self._source.get_data())
        self._queue_A.put(img, timeout=self._timeout)
        logger.info(f"{self.ROLE.capitalize()} produced and passed image.")
        time.sleep(SLEEP_SEC)
