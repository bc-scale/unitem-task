import logging
from pathlib import Path
from queue import Queue
from time import time

from PIL.Image import Image

from src.service.actor import Actor

logger = logging.getLogger("custom_logger")


class ImgSaver(Actor):
    """Thread taking an image from queue *B*
    and saving in destination directory."""

    def __init__(
        self,
        queue_B: Queue[Image],
        dir_path: Path,
        err_queue: Queue[Exception],
        n_iterations: int,
        timeout: int = 2,
    ):
        super().__init__(
            role="image saver",
            err_queue=err_queue,
            n_iterations=n_iterations,
            timeout=timeout,
        )
        self._queue_B = queue_B
        self._dir_path = dir_path

    def _save_img(self, img: Image) -> None:
        path = self._dir_path / f"{str(time())}.png"
        img.save(path)

    def _action(self) -> None:
        img = self._queue_B.get(timeout=self._timeout)
        self._save_img(img)
        logger.info(f"{self.ROLE.capitalize()} saved image.")
