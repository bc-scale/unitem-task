from typing import Final

import numpy as np
from numpy.typing import NDArray


class Source:
    """Random 3 dimensional NDArray generator."""

    def __init__(self, source_shape: tuple[int, int, int]):
        assert (
            len(source_shape) == 3
        ), f"Source shape should be 3 dimensional, not {len(source_shape)}."
        self.SHAPE: Final[tuple[int, int, int]] = source_shape

    def get_data(self) -> NDArray[np.uint8]:
        """Generates 3d data, with shape same as sources *SHAPE*.

        Returns
        -------
        NDArray[np.uint8]
            Random 3d data.
        """
        return np.random.randint(256, size=(self.SHAPE), dtype=np.uint8)
