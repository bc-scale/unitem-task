import logging
from abc import ABC, abstractmethod
from queue import Queue
from threading import Thread
from typing import Final

logger = logging.getLogger("custom_logger")


class Actor(Thread, ABC):
    def __init__(
        self,
        role: str,
        err_queue: Queue[Exception],
        n_iterations: int,
        timeout: int = 2,
    ):
        super().__init__()
        self.ROLE: Final[str] = role
        self._n_iterations = n_iterations
        self._timeout = timeout
        self._err_queue = err_queue

    @abstractmethod
    def _action(self) -> None:  # pragma: no cover
        pass

    def _take_action_n_times(self, n: int) -> None:
        for _ in range(n):
            self._action()

    def _pass_error(self, e: Exception) -> None:
        logger.error(
            f"{self.ROLE.capitalize()} caught en error. "
            "Passing exception to error queue."
        )
        self._err_queue.put(e)

    def run(self) -> None:
        logger.debug(f"{self.ROLE.capitalize()} starts.")

        try:
            self._take_action_n_times(self._n_iterations)
        except Exception as e:
            self._pass_error(e)

        logger.info(f"{self.ROLE.capitalize()} ends.")
