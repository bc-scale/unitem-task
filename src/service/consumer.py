import logging
from queue import Queue
from typing import Final

import numpy as np
from PIL.Image import Image, Resampling
from PIL.ImageFilter import MedianFilter

from src.service.actor import Actor

logger = logging.getLogger("custom_logger")
KERNEL_SIZE: Final[int] = 5


class Consumer(Actor):
    """Thread taking an image from queue *A*, resizing by half and image,
    applying median filter and passing result to queue *B*."""

    def __init__(
        self,
        queue_A: Queue[Image],
        queue_B: Queue[Image],
        err_queue: Queue[Exception],
        n_iterations: int,
        timeout: int = 2,
    ):
        super().__init__(
            role="consumer",
            err_queue=err_queue,
            n_iterations=n_iterations,
            timeout=timeout,
        )
        self._queue_A = queue_A
        self._queue_B = queue_B

    def _process_img(self, img: Image) -> Image:
        width, height = img.size
        img = img.resize(
            (int(width / np.sqrt(2)), int(height / np.sqrt(2))),
            resample=Resampling.NEAREST,
        )
        return img.filter(MedianFilter(size=KERNEL_SIZE))

    def _action(self) -> None:
        img = self._queue_A.get(timeout=self._timeout)
        processed_img = self._process_img(img)
        self._queue_B.put(processed_img, timeout=self._timeout)
        logger.info(f"{self.ROLE.capitalize()} processed and passed image.")
