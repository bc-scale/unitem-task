import logging
from pathlib import Path
from queue import Queue

from PIL.Image import Image

from src.service.consumer import Consumer
from src.service.img_saver import ImgSaver
from src.service.producer import Producer
from src.service.source import Source

logger = logging.getLogger("custom_logger")
THREAD_WAIT_TIME = 0.05


class Service:
    """Multithread service generating, processing and saving images.

    Data pipeline:
    Producer thread get data from *source*, casts it to an *Pillow Image*
    and pass using *A* queue. Next, consumer thread takes the image, process
    and pass result to queue *B*. Finally, image saver thread takes transformed
    image and save it to indicated directory.

    Service ends running if:
    - all child threads finished work (data pipeline run *n* full iterations),
    - any exception in child thread was raised.

    In the second case, to be able to process raised exception, child thread
    pass it using *_err_queue*. After receiving it service exit waiting loop
    and raise first registered child thread exception in main (service) thread.
    """

    def __init__(
        self,
        source_shape: tuple[int, int, int],
        n_imgs: int,
        timeout: int = 2,
        dir_path: Path = Path("processed"),
    ):
        self._n_imgs = n_imgs
        self._source = Source(source_shape)
        self._queue_A: Queue[Image] = Queue(maxsize=n_imgs)
        self._queue_B: Queue[Image] = Queue(maxsize=n_imgs)
        self._err_queue: Queue[Exception] = Queue()

        self._producer_thread = Producer(
            source=self._source,
            queue_A=self._queue_A,
            err_queue=self._err_queue,
            n_iterations=n_imgs,
            timeout=timeout,
        )
        self._consumer_thread = Consumer(
            queue_A=self._queue_A,
            queue_B=self._queue_B,
            err_queue=self._err_queue,
            n_iterations=n_imgs,
            timeout=timeout,
        )
        self._img_saver_thread = ImgSaver(
            queue_B=self._queue_B,
            dir_path=dir_path,
            err_queue=self._err_queue,
            n_iterations=n_imgs,
            timeout=timeout,
        )

        self._producer_thread.daemon = True
        self._consumer_thread.daemon = True
        self._img_saver_thread.daemon = True

    def _log_start(self) -> None:
        height, width, _ = self._source.SHAPE
        logger.debug(
            f"Service starting. Order to produce {self._n_imgs} images "
            f"with resolution {width}x{height}."
        )

    def _any_thread_running(self) -> bool:
        any_alive = any(
            [
                self._producer_thread.is_alive(),
                self._consumer_thread.is_alive(),
                self._img_saver_thread.is_alive(),
            ]
        )
        return any_alive

    def _no_error_occured(self) -> bool:
        return self._err_queue.empty()

    def _start(self) -> None:
        self._log_start()
        self._producer_thread.start()
        self._consumer_thread.start()
        self._img_saver_thread.start()

    def _wait(self) -> None:
        self._producer_thread.join(THREAD_WAIT_TIME)
        self._consumer_thread.join(THREAD_WAIT_TIME)
        self._img_saver_thread.join(THREAD_WAIT_TIME)

    def run(self) -> None:
        """Runs application service. Starts producer, consumer and image saver
        threads and waits until all finish or one of them pass en error
        to error queue. If any was passed, the first registered exception
        is raised.

        Raises:
            Exception: any exception raised and passed by child thread."""
        self._start()

        while self._any_thread_running() and self._no_error_occured():
            self._wait()

        if not self._err_queue.empty():
            logger.error("Error received. Service raises exception and ends.")
            raise self._err_queue.get()

        logger.info("Service ends.")
