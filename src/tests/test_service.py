from pathlib import Path

import pytest

from src.service.service import Service


def test_run(queue_len: int, tmp_path: Path) -> None:
    service = Service(
        source_shape=(10, 10, 3), n_imgs=queue_len, dir_path=tmp_path
    )
    service.run()
    files = list(tmp_path.iterdir())
    assert len(files) == queue_len

    with pytest.raises(TypeError):
        Service(source_shape=(10, 10, 1), n_imgs=queue_len).run()
