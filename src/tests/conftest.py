from queue import Queue
from typing import Any, Callable

from PIL.Image import Image, fromarray
from pytest import fixture

from src.service.source import Source

SOURCE_SHAPE: tuple[int, int, int] = (256, 512, 3)


@fixture(scope="session")
def queue_len() -> int:
    return 5


@fixture(scope="session")
def source_shape() -> tuple[int, int, int]:
    return SOURCE_SHAPE


@fixture(scope="session")
def source() -> Source:
    return Source(SOURCE_SHAPE)


def generate_img() -> Image:
    source = Source(SOURCE_SHAPE)
    img = fromarray(source.get_data())
    return img


def fill_queue(
    q: Queue[Any],
    data_generator: Callable[[], Any] = Source(SOURCE_SHAPE).get_data,
) -> Queue[Any]:
    while not q.full():
        q.put(data_generator())
    return q


def empty_queue(q: Queue[Any]) -> Queue[Any]:
    while not q.empty():
        q.get()
    return q
