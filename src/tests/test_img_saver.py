from queue import Empty, Queue

import pytest
from pytest import FixtureRequest, fixture

from src.service.img_saver import ImgSaver
from src.tests.conftest import empty_queue, fill_queue, generate_img


@fixture(scope="class")
def saver(
    request: FixtureRequest,
    queue_len: int,
    tmp_path_factory: pytest.TempPathFactory,
) -> None:
    request.cls.saver = ImgSaver(
        queue_B=Queue(queue_len),
        dir_path=tmp_path_factory.mktemp("temp_data_dir"),
        err_queue=Queue(),
        n_iterations=queue_len,
        timeout=2,
    )


@pytest.mark.usefixtures("saver")
class TestImgSaver:
    saver: ImgSaver

    def test_action(self, queue_len: int) -> None:
        empty_queue(self.saver._queue_B)
        with pytest.raises(Empty):
            self.saver._action()

        fill_queue(self.saver._queue_B, generate_img)
        self.saver._action()
        assert not self.saver._queue_B.full()

        while not self.saver._queue_B.empty():
            self.saver._action()
        files = list(self.saver._dir_path.iterdir())
        assert len(files) == queue_len
        assert all([f.is_file() for f in files])
        assert all([f.suffix == ".png" for f in files])
