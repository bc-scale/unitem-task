from pathlib import Path
from queue import Empty, Full, Queue

import numpy as np
import pytest
from PIL import Image
from pytest import FixtureRequest, fixture

from src.service.consumer import Consumer
from src.service.source import Source
from src.tests.conftest import empty_queue, fill_queue, generate_img


@fixture(scope="class")
def consumer(request: FixtureRequest, queue_len: int) -> None:
    request.cls.cons = Consumer(
        queue_A=Queue(queue_len),
        queue_B=Queue(queue_len),
        err_queue=Queue(),
        n_iterations=queue_len,
        timeout=2,
    )


@pytest.mark.usefixtures("consumer")
class TestConsumer:
    cons: Consumer

    def test_action(self) -> None:

        # check if raise exception
        # when queue A is empty and timeout was reached
        empty_queue(self.cons._queue_A)
        with pytest.raises(Empty):
            self.cons._action()

        # check if cons action remove image from queue A and put on B
        fill_queue(self.cons._queue_A, generate_img)
        empty_queue(self.cons._queue_B)
        self.cons._action()
        assert not self.cons._queue_A.full()
        assert not self.cons._queue_B.empty()

        # check if raise exception
        # when queue B is full and timeout was reached
        if self.cons._queue_A.full():
            self.cons._queue_A.get()
        fill_queue(self.cons._queue_B, generate_img)
        with pytest.raises(Full):
            self.cons._action()

    def test_process_img(self) -> None:
        """Testing method by checking resizing effect
        and error raising when data shape is invalid."""

        def process_img(shape: tuple[int, int, int]) -> Image.Image:
            img = Image.fromarray(Source(shape).get_data())
            new_img = self.cons._process_img(img)
            return new_img

        process_img((10, 10, 2))
        process_img((10, 10, 3))
        process_img((10, 10, 4))

        new_img = process_img((512, 1024, 3))
        assert new_img.size == (724, 362)

        # 1 channel not allowed
        with pytest.raises(TypeError):
            process_img((10, 10, 1))

        # above 4 channels not allowed
        with pytest.raises(TypeError):
            process_img((10, 10, 5))

        # too small image
        with pytest.raises(ValueError):
            process_img((1, 10, 3))

    def test_process_img_with_examples(self) -> None:
        """Test *process_img* on deterministically generated images."""
        data_path = Path("src", "tests", "data")

        def test_example(dir_name: str) -> None:
            in_img = Image.open(data_path / dir_name / "input.png")
            expected_out_img = Image.open(data_path / dir_name / "outcome.png")

            out_img = self.cons._process_img(in_img)
            assert np.array_equal(np.array(out_img), np.array(expected_out_img))

        test_example("salt_and_pepper")
        test_example("lines")
