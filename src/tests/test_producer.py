from queue import Full, Queue

import pytest
from pytest import FixtureRequest, fixture

from src.service.producer import Producer
from src.service.source import Source
from src.tests.conftest import empty_queue, fill_queue, generate_img


@fixture(scope="class")
def producer(request: FixtureRequest, queue_len: int, source: Source) -> None:
    request.cls.prod = Producer(
        source=source,
        queue_A=Queue(queue_len),
        err_queue=Queue(),
        n_iterations=queue_len,
        timeout=2,
    )


@pytest.mark.usefixtures("producer")
class TestProducer:
    prod: Producer

    def test_action(self) -> None:
        # check if producer action add image to queue
        empty_queue(self.prod._queue_A)
        self.prod._action()
        assert not self.prod._queue_A.empty()

        # check if producer raise exception
        # when queue is full and timeout was reached
        fill_queue(self.prod._queue_A, generate_img)
        with pytest.raises(Full):
            self.prod._action()
