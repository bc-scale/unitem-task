from click.testing import CliRunner

from src.main import main


def test_cli() -> None:
    runner = CliRunner()

    result = runner.invoke(main, ["-n", "-1"])
    assert result.exit_code == 2

    result = runner.invoke(main, ["-w", "9"])
    assert result.exit_code == 2

    result = runner.invoke(main, ["-h", "9"])
    assert result.exit_code == 2

    result = runner.invoke(main, ["-l", "infoo"])
    assert result.exit_code == 2

    # correct input
    result = runner.invoke(
        main, ["-n", "2", "-w", "100", "-h", "100", "-l", "INFO"]
    )
    assert result.exit_code == 0
