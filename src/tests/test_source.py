import numpy as np
import pytest

from src.service.source import Source


def test_get_data(source: Source) -> None:
    data = source.get_data()
    assert type(data) == np.ndarray
    assert len(data.shape) == 3
    assert data.shape == source.SHAPE
    assert data.dtype == np.uint8
    assert np.all((data >= 0) & (data < 256))

    with pytest.raises(AssertionError):
        Source(source_shape=(10, 10))  # type: ignore

    with pytest.raises(AssertionError):
        Source(source_shape=(10, 10, 10, 10))  # type: ignore
