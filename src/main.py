import click
from click import IntRange
from click_loglevel import LogLevel

from src.service.service import Service
from src.utils.logger import get_logger


@click.command()
@click.option(
    "-n",
    "--n-imgs",
    type=IntRange(min=1),
    default=10,
    show_default=True,
    help="Number of images to produce.",
)
@click.option(
    "-w",
    "--width",
    type=IntRange(min=10, max=10000),
    default=1024,
    show_default=True,
    help="Image width (pixels).",
)
@click.option(
    "-h",
    "--height",
    type=IntRange(min=10, max=10000),
    default=512,
    show_default=True,
    help="Image height (pixels).",
)
@click.option(
    "-l",
    "--log-lvl",
    type=LogLevel(),
    default="DEBUG",
    show_default=True,
    help="Logging level.",
)
def main(n_imgs: int, width: int, height: int, log_lvl: LogLevel) -> None:
    """Application producing, processing and saving images."""
    _ = get_logger("custom_logger", level=log_lvl)

    service = Service(n_imgs=n_imgs, source_shape=(height, width, 3))
    service.run()


if __name__ == "__main__":
    main()
