# BUILD STAGE
FROM python:3.9.15-slim-buster AS builder

# instaling poetry
RUN apt-get update && \
    apt-get install curl -y && \
    curl -sSL https://install.python-poetry.org | python3 -
ENV PATH="/root/.local/bin:$PATH"

# building app
WORKDIR /app
COPY pyproject.toml poetry.lock poetry.toml /app/
COPY src /app/src
RUN poetry install --only main

# DEPLOY STAGE
FROM python:3.9.15-slim-buster

# Coping venv
COPY --from=builder app app

# Changing workdir
WORKDIR /app

# creating output directry
RUN mkdir processed

# Register least privileged user
RUN groupadd -r my_user_group && \
    useradd -g my_user_group my_user
RUN chown -R my_user:my_user_group /app
USER my_user

# defining entrypoint and default agruments
ENTRYPOINT [ ".venv/bin/python", "src/main.py" ]
CMD [ "--n-imgs", "5", "--width", "1024", "--height", "512", "--log-lvl", "DEBUG" ]
