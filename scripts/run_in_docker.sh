#!/bin/bash

if [[ "$(docker images -q unitem-task 2> /dev/null)" == "" ]]; then
    echo -e "====== BUILDING IMAGE ======\n"
    docker build . -t unitem-task
    echo -e "\n"
fi

echo -e "\t====== RUNNING APP IN DOCKER ======\n"
docker run -v $(pwd)/processed:/app/processed -it unitem-task $@
