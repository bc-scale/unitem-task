from pathlib import Path

import numpy as np
from PIL import Image

data_path = Path("src", "tests", "data")
data_path.mkdir(exist_ok=True)

# gray image with salt and papper
test_path = data_path / "salt_and_pepper"
test_path.mkdir(exist_ok=True)
img = np.ones((362, 724, 3), dtype=np.uint8) * 128
Image.fromarray(img).save(test_path / "outcome.png")

noise = np.random.randn(362, 724)
img[noise > 2.3] = 255
img[noise < -2.3] = 0
Image.fromarray(img).resize((1024, 512), Image.Resampling.NEAREST).save(
    test_path / "input.png"
)

# one color image with lines
test_path = data_path / "lines"
test_path.mkdir(exist_ok=True)
img = np.zeros((362, 724, 3), dtype=np.uint8)
img[:, :] = [40, 60, 100]
Image.fromarray(img).save(test_path / "outcome.png")

img[[50, 181, 312]] = [250, 150, 150]
img[:, [100, 362, 624]] = [150, 250, 150]
Image.fromarray(img).resize((1024, 512), Image.Resampling.NEAREST).save(
    test_path / "input.png"
)
