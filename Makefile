setup_venv:
	poetry install --no-root

install_pre_commit:
	poetry run pre-commit install

install_dev: setup_venv install_pre_commit

isort:
	poetry run isort src

black:
	poetry run black --config pyproject.toml src

flake8:
	poetry run flake8 src

format: isort black

mypy:
	poetry run mypy --incremental --install-types --show-error-codes --pretty src

pre_commit:
	poetry run pre-commit run -a -c .pre-commit-config.yaml

test:
	poetry run pytest src

test_cov:
	poetry run coverage run -m pytest src --cov-config=.coveragerc --junit-xml=junit/test-results.xml --cov-report=html --cov-report=xml
	poetry run coverage html
	poetry run coverage xml
	poetry run coverage report --show-missing

compile_env:
	poetry lock --no-update

build: isort black pre_commit flake8 mypy test

run:
	poetry run prod-cons -n 10 -w 1024 -l 512 -l DEBUG

docker_build:
	docker build . -t unitem-task
